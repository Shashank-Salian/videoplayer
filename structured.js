const container = document.querySelector('.container')
const media = document.querySelector('#video')
const playPauseBtn = document.querySelector('.playPauseBtn')
const playPauseImg = document.querySelector('.playPauseImg')
const fullScreenBtn = document.querySelector('.fullscreenBtn')
const fullScreenImg = document.querySelector('.fullscreenImg')
const prog = document.querySelector('.progress')
const progCont = document.querySelector('.progressContainer')
const progBar = document.querySelector('.progressbar')
const timer = document.querySelector('.timer')
const volProgCont = document.querySelector('.volProgContainer')
const volProg = document.querySelector('.volProgress')
const volBtn = document.querySelector('.volBtn')
const volImg = document.querySelector('.volImg')

playPauseBtn.addEventListener('click', playPauseBtnClickHandler)

const playPauseBtnClickHandler = () => {
	if (playPauseBtn.getAttribute('data-control') === 'play') {
		media.play()
		return
		// onPlayVideo()
	}
	media.pause()
	// onPauseVideo()
}

let id = null;
const onPlayVideo = () => {
	playPauseBtn.setAttribute('data-control', 'pause');
	playPauseImg.src = './Assets/pause.svg'
	playPauseImg.alt = 'pause'
	id = window.setInterval(updateProgress, 300)
}

const updateProgress = () => {
	if (!media.ended) {
		let size = (media.currentTime / media.duration) * 100
		prog.style.width = `${size}%`
		
		const min = Math.floor(media.currentTime / 60)
		const hour = Math.floor(min / 60)
		const sec = Math.floor(media.currentTime % 60)
		timer.innerText = `${hour>0 ? `${hour} : `:''}${min>=10 ? min:`0${min}`} : ${sec>=10 ? sec:`0${sec}`}`
	}
}

const onPauseVideo = () => {
	playPauseBtn.setAttribute('data-control', 'play');
	playPauseImg.src = './Assets/play.svg'
	playPauseImg.alt = 'play'
	window.clearInterval(id)
	id  = null
}

media.addEventListener('play', onPlayVideo)
media.addEventListener('pause', onPauseVideo)

progCont.addEventListener('mousemove', onProgMouseMove)
progCont.addEventListener('mouseout', onProgMouseOut)
progCont.addEventListener('click', onProgClick)

const onProgMouseMove = e => {
	const rect = e.target.getBoundingClientRect()
	progBar.style.width = `${e.clientX - rect.left}px`
}

const onProgMouseOut = () => {
	progBar.style.width = `0px`
}

const onProgClick = e => {
	const rect = progCont.getBoundingClientRect()
	//					Get current percentage of progress and then find the duration for that percentage
	const pos = ((e.clientX - rect.left) / progCont.offsetWidth)*100
	media.currentTime = (pos*media.duration) / 100
	updateProgress()
}

volProgCont.addEventListener('click', onVolProgClick)

const onVolProgClick = (arg) => {
	const rect = volProgCont.getBoundingClientRect()
	if (typeof(arg) === "number") {
		arg = { clientX: (arg*100)+rect.left }
	}
	const perc = (arg.clientX - rect.left)
	volProg.style.width = `${perc}px`
	media.volume = (perc / 100)
	if (media.volume && volBtn.getAttribute('data-control') != 'mute') onUnMute()
	if (!media.volume && volBtn.getAttribute('data-control') === 'mute') onMute()
}

const onMute = () => {
	prevVol = media.volume
	volBtn.setAttribute('data-control', 'unmute')
	volImg.src = './Assets/mute.svg'
	volImg.alt = 'Mute'
	if (media.volume)
	onVolProgClick(0)
}

const onUnMute = () => {
	volBtn.setAttribute('data-control', 'mute')
	volImg.src = './Assets/volume.svg'
	volImg.alt = 'Volume'
	if (!media.volume)
	onVolProgClick(prevVol)
}

onVolProgClick(media.volume)

volBtn.addEventListener('click', onVolBtnClick)

const onVolBtnClick = () => {
	if (volBtn.getAttribute('data-control') === 'mute') {
		onMute()
		return
	}
	onUnMute()
}

media.addEventListener('ended', onVideEnd)

const onVideEnd = () => {
	prog.style.width = '100%'
	onPauseVideo()
}

fullScreenBtn.addEventListener('click', fullScreenBtnClickHandler)

const fullScreenBtnClickHandler = () => {
	if (fullScreenBtn.getAttribute('data-control') === 'fullscreen') {
		container.requestFullscreen()
		return
	}
	document.exitFullscreen()
}

const onSetFullScreen = () => {
	fullScreenBtn.setAttribute('data-control', 'exitFullScreen')
	fullScreenImg.src = './Assets/exitFullscreen.svg'
	fullScreenImg.alt = 'exit fullscreen'
}

const onExitFullScreen = () => {
	fullScreenBtn.setAttribute('data-control', 'fullscreen')
	fullScreenImg.src = './Assets/fullscreen.svg'
	fullScreenImg.alt = 'fullscreen'
}

document.addEventListener('fullscreenchange', () => {
	if (document.fullscreenElement) {
		onSetFullScreen()
		return
	}
	onExitFullScreen()
})

document.addEventListener('keypress', onKeyPress)

const onKeyPress = ({ key }) => {
	switch (key) {
		case 'k':
		case ' ':
			playPauseBtnClickHandler()
			break
		case 'f':
			fullScreenBtnClickHandler()
			break
		case 'm':
			onVolBtnClick()
			break
		default:
			break
	}
}
