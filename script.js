const container = document.querySelector('.container')
const media = document.querySelector('#video')
const playPauseBtn = document.querySelector('.playPauseBtn')
const playPauseImg = document.querySelector('.playPauseImg')
const fullScreenBtn = document.querySelector('.fullscreenBtn')
const fullScreenImg = document.querySelector('.fullscreenImg')
const prog = document.querySelector('.progress')
const progCont = document.querySelector('.progressContainer')
const progBar = document.querySelector('.progressbar')
const timer = document.querySelector('.timer')
const volProgCont = document.querySelector('.volProgContainer')
const volProg = document.querySelector('.volProgress')
const volBtn = document.querySelector('.volBtn')
const volImg = document.querySelector('.volImg')

let id = null, prevVol = 1;

const updateProgress = () => {
	if (!media.ended) {
		let size = (media.currentTime / media.duration) * 100
		prog.style.width = `${size}%`
		
		const min = Math.floor(media.currentTime / 60)
		const hour = Math.floor(min / 60)
		const sec = Math.floor(media.currentTime % 60)
		timer.innerText = `${hour>0 ? `${hour} : `:''}${min>=10 ? min:`0${min}`} : ${sec>=10 ? sec:`0${sec}`}`
	}
}

const onPlayVideo = () => {
	// if (!id) {
		playPauseBtn.setAttribute('data-control', 'pause');
		playPauseImg.src = './Assets/pause.svg'
		playPauseImg.alt = 'pause'
		id = window.setInterval(updateProgress, 300)
	// }
}

const onPauseVideo = () => {
	// if (id) {
		playPauseBtn.setAttribute('data-control', 'play');
		playPauseImg.src = './Assets/play.svg'
		playPauseImg.alt = 'play'
		window.clearInterval(id)
		id  = null
	// }
}

const onSetFullScreen = () => {
	fullScreenBtn.setAttribute('data-control', 'exitFullScreen')
	fullScreenImg.src = './Assets/exitFullscreen.svg'
	fullScreenImg.alt = 'exit fullscreen'
}

const onExitFullScreen = () => {
	fullScreenBtn.setAttribute('data-control', 'fullscreen')
	fullScreenImg.src = './Assets/fullscreen.svg'
	fullScreenImg.alt = 'fullscreen'
}

const playPauseBtnClickHandler = () => {
	if (playPauseBtn.getAttribute('data-control') === 'play') {
		media.play()
		return
		// onPlayVideo()
	}
	media.pause()
	// onPauseVideo()
}

const fullScreenBtnClickHandler = () => {
	if (fullScreenBtn.getAttribute('data-control') === 'fullscreen') {
		container.requestFullscreen()
		return
	}
	document.exitFullscreen()
}

const onProgMouseMove = e => {
	const rect = e.target.getBoundingClientRect()
	progBar.style.width = `${e.clientX - rect.left}px`
}

const onProgClick = e => {
	//					Get current percentage of progress and then find the duration for that percentage
	const rect = progCont.getBoundingClientRect()
	media.currentTime = ((((e.clientX - rect.left) / progCont.offsetWidth)*100)*media.duration) / 100
	updateProgress()
}

const onProgMouseOut = () => {
	progBar.style.width = `0px`
}

const onVolProgClick = (arg) => {
	const rect = volProgCont.getBoundingClientRect()
	if (typeof(arg) === "number") {
		arg = { clientX: (arg*100)+rect.left }
	}
	const perc = (arg.clientX - rect.left)
	volProg.style.width = `${perc}px`
	console.log(perc/100)
	media.volume = (perc / 100)
	if (media.volume && volBtn.getAttribute('data-control') != 'mute') onUnMute()
	if (!media.volume && volBtn.getAttribute('data-control') === 'mute') onMute()
}

const onMute = () => {
	prevVol = media.volume
	volBtn.setAttribute('data-control', 'unmute')
	volImg.src = './Assets/mute.svg'
	volImg.alt = 'Mute'
	if (media.volume)
	onVolProgClick(0)
}

const onUnMute = () => {
	volBtn.setAttribute('data-control', 'mute')
	volImg.src = './Assets/volume.svg'
	volImg.alt = 'Volume'
	if (!media.volume)
	onVolProgClick(prevVol)
}

const volumeChange = (vol) => {
	console.log(vol)
}

const onVolBtnClick = () => {
	if (volBtn.getAttribute('data-control') === 'mute') {
		onMute()
		return
	}
	onUnMute()
}

const onVideEnd = () => {
	prog.style.width = '100%'
	onPauseVideo()
}

const onKeyPress = ({ key }) => {
	switch (key) {
		case 'k':
		case ' ':
			playPauseBtnClickHandler()
			break
		case 'f':
			fullScreenBtnClickHandler()
			break
		case 'm':
			onVolBtnClick()
			break
		default:
			break
	}
}

document.addEventListener('fullscreenchange', () => {
	if (document.fullscreenElement) {
		onSetFullScreen()
		return
	}
	onExitFullScreen()
})
media.addEventListener('click', playPauseBtnClickHandler)
media.addEventListener('ended', onVideEnd)
media.addEventListener('play', onPlayVideo)
media.addEventListener('pause', onPauseVideo)
media.addEventListener('contextmenu', e => e.preventDefault())
// media.addEventListener('volumechange', onMediaVolChange)
// media.addEventListener('loadstart', onVideoLoadStart)
// media.addEventListener('canplay', onVideoLoadStart)
// media.addEventListener('load', onVideoLoadStart)
onVolProgClick(media.volume)

progCont.addEventListener('mousemove', onProgMouseMove)
progCont.addEventListener('mouseout', onProgMouseOut)
progCont.addEventListener('click', onProgClick)

fullScreenBtn.addEventListener('click', fullScreenBtnClickHandler)
playPauseBtn.addEventListener('click', playPauseBtnClickHandler)
volProgCont.addEventListener('click', onVolProgClick)
volBtn.addEventListener('click', onVolBtnClick)

document.addEventListener('keypress', onKeyPress)
